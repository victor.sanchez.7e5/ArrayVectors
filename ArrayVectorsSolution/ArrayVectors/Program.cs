﻿using System;

public class Program
{

    public void Menu()
    {
        string opcio = "";
        do
        {

            Console.WriteLine("1- Day Of Week");
            Console.WriteLine("2- Player Numbers");
            Console.WriteLine("3- Candidates List ");
            Console.WriteLine("4- Letter in Word     ");
            Console.WriteLine("5- AddValuesToList    ");
            Console.WriteLine("6- Swap    ");
            Console.WriteLine("7- PushButtonPadlockSimulator  ");
            Console.WriteLine("8- BoxesOpenedCounter  ");
            Console.WriteLine("9- MinOf10Values  ");
            Console.WriteLine("10- IsThereaMultipleOf 7  ");
            Console.WriteLine("11- SearchInOrdered   ");

            
            Console.WriteLine("0 - Sortir");
            opcio = Console.ReadLine();

            switch (opcio)
            {
                case "1":
                    DayOfWeek();
                    break;
                case "2":
                    PlayerNumbers();
                    break;
                case "3":
                    CandidatesList();
                    break;
                case "4":
                    LetterInWord();
                    break;
                case "5":
                    AddValuesToList();
                    break;
                case "6":
                    Swap();
                    break;
                case "7":
                    PushButtonPadlockSimulator();
                    break;
                case "8":
                    BoxesOpenedCounter();
                    break;
                case "9":
                    MinOf10Values();
                    break;
                case "10":
                    IsThereaMultipleOf7();
                    break;
                case "11":
                    SearchInOrdered();
                    break; 
                case "0":
                    Console.WriteLine("Press Enter to end the program"); break;
                default: Console.WriteLine("Opcio Incorrecta"); break;
            }
        } while (opcio != "0");
    }

    private static void Main(string[] args)
    {
        var menu = new Program();
        menu.Menu();
    }
    //Donat un enter, printa el dia de la setmana amb text (dilluns, dimarts, dimecres…), tenint en compte que dilluns és el 0.
    //Els dies de la setmana es guarden en un vector.

    public void DayOfWeek()
    {

        Console.WriteLine("Escriu un numero del 1 al 7 que representara el dia de la setmana");

        int numdia;
        string[] weekdays;
        weekdays = new string[7] { "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge" };

        numdia = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine(weekdays[numdia - 1]);


    }

    public void PlayerNumbers()
    {
        int jugador1 = Convert.ToInt32(Console.ReadLine());
        int jugador2 = Convert.ToInt32(Console.ReadLine());
        int jugador3 = Convert.ToInt32(Console.ReadLine());
        int jugador4 = Convert.ToInt32(Console.ReadLine());
        int jugador5 = Convert.ToInt32(Console.ReadLine());

        int[] jugadors = new int[5] { jugador1, jugador2, jugador3, jugador4, jugador5 };

        Console.WriteLine($"[{jugadors[0]},{jugadors[1]},{jugadors[2]},{jugadors[3]},{jugadors[4]}]");
    }

    public void CandidatesList()
    {
        Console.WriteLine("Escriu un nombre de candidats:");
        int numCandidates = Convert.ToInt32(Console.ReadLine());


        string nomCandidat = "";
        string[] nomCandidates = new string[numCandidates];
        int pos = 0;


        for (int i = 0; i < nomCandidates.Length; i++)
        {
            Console.WriteLine($"Nom Candidat {i}: ");
            nomCandidat = Console.ReadLine();

            nomCandidates[i] = nomCandidat;
        }




        for (int i = 0; i < nomCandidates.Length; i++)
        {
            for (int j = 0; pos != -1; i++)
            {
                nomCandidates[j] = nomCandidates[pos];
                Console.Write("Indica les posicions de la llista ");
                pos = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine(nomCandidates[pos]);      //comprovar si mostra el candidat a la posició indicada o si dona error 

        }

    }


    public void LetterInWord()
    {

        string paraula = Console.ReadLine();
        int posicio = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine(paraula[posicio - 1]);
    }

    public void AddValuesToList()
    {

        float[] vector = new float[50];

        for (int i = 0; i < vector.Length; i++) vector[i] = 0.0f;

        vector[0] = 31.0f;
        vector[1] = 56.0f;
        vector[19] = 12.0f;
        vector[49] = 79.0f;

        Console.Write("[");
        for (int i = 0; i < vector.Length; i++)
        {
            Console.Write($"{vector[i]},");
        }
        Console.Write("]");
    }

    /// <summary>
    /// Donat un vector de 4 números de tipus int,intercanvia el primer per l'últim element
    /// </summary>
    public void Swap()
    {
        int[] numeros = new int[4] { 9, 4, 6, 7 };
        int numeroChangeAux;

        int i;

        Console.WriteLine("[");
        for (i = 0; i < numeros.Length; i++)
        {
            Console.Write(numeros[i]);
        }
        Console.WriteLine("]");

        numeroChangeAux = numeros[0];
        numeros[0] = numeros[3];
        numeros[3] = numeroChangeAux;

        Console.WriteLine("[");
        for (i = 0; i < numeros.Length; i++)
        {
            Console.Write(numeros[i]);
        }
        Console.WriteLine("]");
    }


    public void PushButtonPadlockSimulator()
    {
        int numsCombination = 0;
        int countInputs = 0;
        int[] candau = new int[8] { 0, 1, 2, 3, 4, 5, 6, 7 };
        bool[] isActivatedButton;
        isActivatedButton = new bool[8];

        Console.WriteLine("Escriu la teva combinació de números del 0 al 7 i en acabar escriu -1");
        // si el boto es prem dos cops a la primera s'activa i a la segona es desactiva

        Console.WriteLine("Contraseña : ");
        do
        {
            Console.WriteLine("* ");

            numsCombination = Convert.ToInt32(Console.ReadLine());
            if (numsCombination == -1) break;

            if (isActivatedButton[numsCombination] == true) isActivatedButton[numsCombination] = false;

            if (numsCombination == candau[numsCombination]) isActivatedButton[numsCombination] = true;

            else isActivatedButton[numsCombination] = false;


        } while (numsCombination != -1 || numsCombination > 7 || numsCombination < -1);


        Console.WriteLine("[");
        for (int i = 0; i < isActivatedButton.Length; i++)
        {
            Console.Write(isActivatedButton[i] + " ,");
        }
        Console.WriteLine("]");

    }

    public void BoxesOpenedCounter()
    {

        int[] caixes = new int[11] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        int countTimesOpened = 0;

        int[] caixesTimesOpened = new int[11] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        int boxOpened = 0;

        Console.WriteLine("Introdueix enters del 0 al 10 quan s'obri la caixa indicada.\n");


        for (int i = 0; boxOpened != -1; i++)
        {
            do
            {
                boxOpened = Convert.ToInt32(Console.ReadLine());
                if (boxOpened > 10 || boxOpened < -1) Console.WriteLine("ERROR -- No pot ser major que 10 ni menor que 0");
            } while (boxOpened > 10 || boxOpened < -1);

            if (boxOpened == -1) break;

            if (caixes[boxOpened] == boxOpened) caixesTimesOpened[boxOpened]++;
        }


        Console.WriteLine("[");
        for (int i = 0; i < caixesTimesOpened.Length; i++)
        {
            Console.Write(caixesTimesOpened[i] + " , ");
        }
        Console.WriteLine("]");

    }

    /// <summary>
    /// L'usuari entra 10 enters. Crea un vector amb aquest valors. Imprimeix per pantalla el valor més petit introduït.
    /// </summary>
    public void MinOf10Values()
    {
        int[] enters = new int[10];
        int MinValue;

        Console.WriteLine("L'usuari entra 10 enters :");
        for (int i = 0; i < enters.Length; i++) enters[i] = Convert.ToInt32(Console.ReadLine());

        MinValue = enters[0];

        for (int i = 0; i < enters.Length; i++)
        {
            if (enters[i] < MinValue)
            {
                MinValue = enters[i];
            }
        }
        Console.WriteLine("El valor mínim : " + MinValue);

    }

    public void IsThereaMultipleOf7()
    {
        int[] values = new int[] { 4, 8, 9, 40, 54, 84, 40, 6, 84, 1, 1, 68, 84, 68, 4, 840, 684, 25, 40, 98, 54, 687, 31, 4894, 468, 46, 84687, 894, 40, 846, 1681, 618, 161, 846, 84687, 6, 848 };
        bool IsDivisible = false;
        int divisor = 7;  

        for (int i = 0; i < values.Length; i++)
        {
            if (values[i] % divisor == 0)
            {
                IsDivisible = true;
                break; 
            }
            else IsDivisible = false; 
        }
        Console.WriteLine(IsDivisible); 

    }

    public void SearchInOrdered()
    {
        int indexEnters; 
        int[] enters ;
        int numberInList;
        bool isOnLinst = false; 

        Console.WriteLine("Introdueix la quantitat de numeros que vols a llista d'enters");
        indexEnters = Convert.ToInt32(Console.ReadLine());
        enters = new int[indexEnters] ;

        Console.WriteLine("Introdueix la llista d'enters ordenats de menor a major");
        for (int i = 0; i < enters.Length; i++) enters[i] = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Valor a comprovar : "); 
        numberInList = Convert.ToInt32(Console.ReadLine());

        for (int i = 0; i < enters.Length; i++) if (numberInList == enters[i]) isOnLinst = true;

        Console.WriteLine(isOnLinst); 
    }

}

